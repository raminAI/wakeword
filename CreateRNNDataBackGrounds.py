import pickle
import numpy as np
import DatasetObj
import matplotlib.pyplot as plt
import IPython.display as ipd
import io
import os
import glob
import IPython
import scipy
import copy
import sys
root = '/mnt/NewVM/ramin/'

sys.path.insert(0, root+'dsptools/NPOnly/')
sys.path.insert(0, root+'dsptools/')
from S_flat import S_flat
from STFT import STFT
from resample import resampleB

desiredFs = 16000
thr = 0.15

###########################################
coughList = []
packageCoughSize = 500
countBig = 0
for countX in range(1,4599):
    if np.random.rand() > thr: 
        print(countX/4599, sep=' ', end='\r', flush=True)
        with open(root+'picklefinalCoughs/AllAugCoughs' + str(countX) + '.pickle', 'rb') as data:
                coughAudio = pickle.load(data)
        for count in range(len(coughAudio)):
            dataCough = np.float64(coughAudio[count].data.squeeze())
            if np.max(dataCough) > 0.01:
                dataCough = dataCough/(np.max(dataCough))
                fs = coughAudio[count].sr
                if len(dataCough)/fs > 0.15:
#                     pspec = STFT(dataCough,fs,2048)
#                     sflat = S_flat(pspec)
#                     if 1:#sflat < 0.18 :
                    dataCough = resampleB(dataCough,fs,16000)
                    coughList.append(np.float64(copy.copy(dataCough)))
        if len(coughList) >= packageCoughSize: 
            with open(root+'pickles/coughList' + str(countBig) + '.pickle', 'wb') as output:
                        pickle.dump(coughList,output)
            countBig += 1
            coughList = []

print(np.shape(coughList))
del(coughList)

###########################################
packageNoiseSize = 100
noiseCutList = []
countBig = 0
for count in range(1,22158):
    if np.random.rand() > thr:
        print(count/22158, sep=' ', end='\r', flush=True)
        with open(root+'picklefinalCoughs/AllAugNoise' + str(count) + '.pickle', 'rb') as data:
                noiseAudioSets = pickle.load(data)
#         if not isinstance(noiseAudioSets, list): 
#             noiseAudioSets = [noiseAudioSets]
        for noiseAudio in noiseAudioSets:
                dataNoise = noiseAudio.data.squeeze()
                fs = noiseAudio.sr
                for i in range(20):
                    if len(dataNoise)/fs >21: 
                        idx = np.random.randint(1,(len(dataNoise)/fs)-11)
                        data = dataNoise[idx*fs:(idx+10)*fs]/np.max(dataNoise[idx*fs:(idx+10)*fs])
#                         if np.max(data) > 0.01:
                        data = data/np.max(data)
                        data = resampleB(data,fs,16000)
                        noiseCutList.append(np.float64(copy.copy(data)))

                    if len(noiseCutList) >= packageNoiseSize: 
                        with open(root+'pickles/noiseCutList' + str(countBig) + '.pickle', 'wb') as output:
                                    pickle.dump(noiseCutList,output)
                        countBig += 1
                        noiseCutList = []

print(np.shape(noiseCutList))
del(noiseCutList)

