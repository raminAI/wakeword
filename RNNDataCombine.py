import pickle
import copy
import numpy as np

TData = np.zeros((0,64,500))
TLabel = np.zeros((0,1,500))
root = '/mnt/NewVM/ramin/'
count = 400
b = [0 100]
for j in count:
    for i in range(b[0],b[1]):
        
        print(i, sep=' ', end='\r', flush=True)

        with open(root+'pickles/trainingExampleList'+ str(i) + '.pickle', 'rb') as data:
            D = pickle.load(data)
        TData = np.concatenate((TData,(copy.copy(D))))
        with open(root+'pickles/trainingLabelList' + str(i) +  '.pickle', 'rb') as data:
            L = pickle.load(data)
        TLabel = np.concatenate((TLabel,copy.copy(L)))


    with open(root+'pickles/trainingExampleAll' + str(j) + '.pickle', 'wb') as output:
            pickle.dump(TData,output, protocol=4)

    with open(root+'pickles/trainingLabelAll' + str(j) + '.pickle', 'wb') as output:
            pickle.dump(TLabel,output, protocol=4)
            
    TData = np.zeros((0,64,500))
    TLabel = np.zeros((0,1,500))
    b[0] = b[0] + 100
    b[1] = b[1] + 100
    
count = 100
for j in count:
    for i in range(b[0],b[1]):

        print(i, sep=' ', end='\r', flush=True)

        with open(root+'pickles/trainingExampleList'+ str(i) + '.pickle', 'rb') as data:
                D = pickle.load(data)
        TData = np.concatenate((TData,(copy.copy(D))))
        with open(root+'pickles/trainingLabelList' + str(i) +  '.pickle', 'rb') as data:
                 L = pickle.load(data)
        TLabel = np.concatenate((TLabel,copy.copy(L)))

    with open(root+'pickles/validationExampleAll' + '.pickle', 'wb') as output:
            pickle.dump(TData,output, protocol=4)
    with open(root+'pickles/validationLabelAll' + '.pickle', 'wb') as output:
            pickle.dump(TLabel,output, protocol=4)
    TData = np.zeros((0,64,500))
    TLabel = np.zeros((0,1,500))
    b[0] = b[0] + 100
    b[1] = b[1] + 100