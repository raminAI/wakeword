import pickle
import numpy as np
import DatasetObj
from RNN_Feats import RNN_Feats
import matplotlib.pyplot as plt
import IPython.display as ipd
import io
import os
import glob
import IPython
from td_utils import *
import scipy
import copy
import sys
root = '/mnt/NewVM/ramin/'
sys.path.insert(0, root+'dsptools/NPOnly/')
sys.path.insert(0, root+'dsptools/')
from S_flat import S_flat
from STFT import STFT
from resample import resampleB

Ty = 500 #label vector

def get_random_time_segment(segment_ms):
    """
    Gets a random time segment of duration segment_ms in a 10,000 ms audio clip.
    
    Arguments:
    segment_ms -- the duration of the audio clip in ms ("ms" stands for "milliseconds")
    
    Returns:
    segment_time -- a tuple of (segment_start, segment_end) in ms
    """
    
    segment_start = np.random.randint(low=0, high=10000-segment_ms-100) # to avoid getting to close subtract another 100 ms
    # Make sure segment doesn't run past the 10sec background 
    segment_end = segment_start + segment_ms - 1
    
    return (segment_start, segment_end)

def is_overlapping(segment_time, previous_segments):
    """
    Checks if the time of a segment overlaps with the times of existing segments.
    
    Arguments:
    segment_time -- a tuple of (segment_start, segment_end) for the new segment
    previous_segments -- a list of tuples of (segment_start, segment_end) for the existing segments
    
    Returns:
    True if the time segment overlaps with any of the existing segments, False otherwise
    """
    
    segment_start, segment_end = segment_time
    
   
    overlap = False
    
    # loop over the previous_segments start and end times.
    # Compare start/end times and set the flag to True if there is an overlap 
    for previous_start, previous_end in previous_segments:
        if previous_end >= segment_start+1000 and previous_start <= segment_end+1000:
            overlap = True

    return overlap

def insert_audio_clip(background, audio_clip, previous_segments, fs):
    """
    Insert a new audio segment over the background noise at a random time step, ensuring that the 
    audio segment does not overlap with existing segments.
    
    Arguments:
    background -- a 10 second background audio recording.  
    audio_clip -- the audio clip to be inserted/overlaid. 
    previous_segments -- times where audio segments have already been placed
    
    Returns:
    new_background -- the updated background audio
    """
    seed = int(abs(np.sum(background)+np.mean(audio_clip)))
    np.random.seed(seed)
    # Get the duration of the audio clip in ms
    segment_ms = len(audio_clip)/fs/1000
    # Use one of the helper functions to pick a random time segment onto which to insert 
    # the new audio clip. 
    segment_time = get_random_time_segment(segment_ms)
    # Check if the new segment_time overlaps with one of the previous_segments. If so, keep 
    # picking new segment_time at random until it doesn't overlap. 
    while is_overlapping(segment_time, previous_segments):
        segment_time = get_random_time_segment(segment_ms)

    # Add the new segment_time to the list of previous_segments
    previous_segments.append(segment_time)
    gain = np.random.randint(1,4)
    # Superpose audio segment and background
    idx = int(segment_time[0]/1000 * fs )
    overlaying  = audio_clip/gain
    sizeBK = len(background[idx:idx+len(overlaying)]) #rounding errors
    background[idx:idx+len(overlaying)] = background[idx:idx+len(overlaying)] + overlaying[0:sizeBK]
    
    return background, segment_time

def insert_ones(y, segment_end_ms):
    """
    Update the label vector y. The labels of the 50 output steps strictly after the end of the segment 
    should be set to 1. By strictly we mean that the label of segment_end_y should be 0 while, the
    50 followinf labels should be ones.
    
    
    Arguments:
    y -- numpy array of shape (1, Ty), the labels of the training example
    segment_end_ms -- the end time of the segment in ms
    
    Returns:
    y -- updated labels
    """
    
    # duration of the background (in terms of spectrogram time-steps)
    segment_end_y = int(segment_end_ms * Ty / 10000.0)
    
    # Add 1 to the correct index in the background label (y)
    for i in range(segment_end_y+1, segment_end_y+51):
        if i < Ty:
            y[0, i] = 1
    
    return y

def create_training_example(background, activates,fs):
    """
    Creates a training example with a given background, activates.
    
    Arguments:
    background -- a 10 second background audio recording
    activates -- a list of audio segments of the word "activate"
    
    Returns:
    x -- the spectrogram of the training example
    y -- the label at each time step of the spectrogram
    """
    
    # Set the random seed
    np.random.seed(int(len(activates)+np.mean(background)))
    snr = np.random.randint(8, 20)
    # Make background quieter
    background = background/snr

    # Initialize y (label vector) of zeros
    y = np.zeros((1,Ty))

    # Initialize segment times as empty list
    previous_segments = []
    
    # Select 0-10 random "activate" audio clips from the entire list of "activates" recordings
    number_of_activates = np.random.randint(1, min(len(activates),10))
    random_indices = np.random.randint(len(activates), size=number_of_activates)
    random_activates = [activates[i] for i in random_indices]
    
    # Step 3: Loop over randomly selected "activate" clips and insert in background
    for random_activate in random_activates:
        # Insert the audio clip on the background
        background, segment_time = insert_audio_clip(background, random_activate, previous_segments,fs)
        # Retrieve segment_start and segment_end from segment_time
        segment_start, segment_end = segment_time
        # Insert labels in "y"
        y = insert_ones(y, segment_end)

    return background,y


numExamples = 50000
np.random.seed(0)
desiredFs = 16000
nfft = 512
countExample = 0
for i in range(numExamples): 
    print(i/numExamples, sep=' ', end='\r', flush=True)
    trainingFeats = []
    trainingLabelList = []
    
    counti = np.random.randint(0,372)   
    with open(root+'pickles/coughList'+ str(counti) + '.pickle', 'rb') as data:
            coughList = pickle.load(data)

    countj = np.random.randint(0,106)   
    with open(root+'pickles/noiseCutList' + str(countj)+ '.pickle', 'rb') as data:
            noiseCutList = pickle.load(data)

    for dataNoiseCut in noiseCutList:
        if np.random.rand() > 0.5:
            TrainingExample,y = create_training_example(dataNoiseCut, coughList,desiredFs)
            X = RNN_Feats(TrainingExample,desiredFs,nfft)
            Xdb = np.power(X,0.23)
            trainingFeats.append(copy.copy(Xdb))
            trainingLabelList.append(y)
    with open(root+'pickles/trainingExampleList' + str(countExample) + '.pickle', 'wb') as output:
                pickle.dump(trainingFeats,output)
    with open(root+'pickles/trainingLabelList' + str(countExample)+ '.pickle', 'wb') as output:
                pickle.dump(trainingLabelList,output)
    countExample += 1

