import sys
root = '/mnt/NewVM/ramin/'
sys.path.insert(0, root+'dsptools/NPOnly/')
import numpy as np 
from STFT import STFT
from makeSmall import makeSmall

def RNN_Feats(data,fs,nfft):
        
    pspec = STFT(data, fs, nfft,0.0025,0.001)
    X0 = makeSmall(np.abs(pspec),64,500)
    return X0