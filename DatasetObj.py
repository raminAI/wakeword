# this class defines the data objects.

class Dataset(object):
    Quality = 1 # quality of the data, initially it's assumed it's good quality
    Pid = 0 # patient id
    Hospital = '' #hospital name
    Weight = 0 # weight
    Age = 0
    Diagnosis = ''
    date = ''
    tag = ''
    minute = 0
    key = ''
    Augmented = 0 #is this an augmnted data? no = 0
    def __init__(self, name, data,timeLen, sr): # constructor 
        self.name = name
        self.data = data
        self.timeLen = timeLen
        self.sr = sr
    def setQuality(self,value):
        self.Quality = value
    def setID(self,Pid):
        self.Pid = Pid
    def setHospital(self,hospital):
        self.Hospital = hospital
    def setWeight(self,weight):
        self.Weight = weight
    def setAge(self,age):
        self.Age = age
    def setDiagnosis(self,diagnosis):
        self.Diagnosis = diagnosis
    def setDate(self,date):
        self.date = date
    def setTag(self,tag):
        self.tag = tag
    def setMins(self,minute):
        self.minute = minute
    def setAug(self,Augmented):
        self.Augmented = Augmented
    def setKey(self,key):
        self.key = key
