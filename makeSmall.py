import numpy as np
def makeSmall(Big,nrows,ncols):
    rows,cols  = np.shape(Big)
    Small = np.zeros((nrows,ncols))
    newRow = [0,int(rows/nrows)]
    for i in range(nrows):
        newCol = [0,int(cols/ncols)]
        for j in range(ncols):
            Small[i][j] = np.mean(Big[newRow[0]:newRow[1],newCol[0]:newCol[1]])
            newCol[0] = newCol[0] + int(cols/ncols)
            newCol[1] = newCol[1] + int(cols/ncols)
        newRow[0] = newRow[0] + int(rows/nrows)
        newRow[1] = newRow[1] + int(rows/nrows)
    return np.abs(Small)

# #toy example
# datax = data[fs:10*fs]
# pspec = librosa.core.stft(datax, n_fft=1024, hop_length=None, win_length=None, window='hann', center=True, pad_mode='reflect')
# pspecABS = np.abs(pspec)
# rows,cols  = np.shape(pspec)
# print(rows)
# print(cols)


# plt.figure(figsize=(15, 3))
# X = pspecABS
# Xdb = np.power(X,0.13)
# libdis.specshow(Xdb, sr=fs, x_axis='time', y_axis='hz')
# plt.colorbar(format='%+2.0f dB')
# plt.tight_layout()
# plt.show()

# x = makeSmall(Xdb,int(rows/10),int(cols/10))
# plt.figure(figsize=(15, 3))
# libdis.specshow(x, sr=fs, x_axis='time', y_axis='hz')
# plt.colorbar(format='%+2.0f dB')
# plt.tight_layout()
# plt.show()